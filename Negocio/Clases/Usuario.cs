﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Clases
{
    public class Usuario
    {
        public string userid { get; set; }
        public string Rut { get; set; }
        public string Nombre { get; set; }

        public string Apellido { get; set; }
        public string Email { get; set; }
        public string Contrasenia { get; set; }
        public string rol { get; set; }
        public string roldescripcion { get; set; }
        public string Fecha { get; set; }

        public RegistroMensaje RegistrarUsuario()
        {
            RegistroMensaje RM = new RegistroMensaje();

            string JsonUser = JsonSerializer.Serialize(this);

            AdminService.AdministradorClient AC = new AdminService.AdministradorClient();

            string JsonRespuesta = AC.Registrar(JsonUser);

            AC.Close();

            RM = JsonSerializer.Deserialize<RegistroMensaje>(JsonRespuesta);


            return RM;
        }

        public RegistroMensaje ResetPassword()
        {
            RegistroMensaje msg = new RegistroMensaje();

            LoginService.LoginServiceClient service = new LoginService.LoginServiceClient();

            string jsonrespuesta = service.ResetPassword(this.Email);

            service.Close();

            msg = JsonSerializer.Deserialize<RegistroMensaje>(jsonrespuesta);

            return msg;
        }

        public RegistroMensaje ChangePassword(string userid,string newpassword)
        {
            RegistroMensaje msg = new RegistroMensaje();

            LoginService.LoginServiceClient service = new LoginService.LoginServiceClient();

            string jsonrespuesta = service.ChangePassword(userid, newpassword);

            service.Close();

            msg = JsonSerializer.Deserialize<RegistroMensaje>(jsonrespuesta);

            return msg;
        }

        public RegistroMensaje EditarUser()
        {
            RegistroMensaje RM = new RegistroMensaje();

            LoginService.LoginServiceClient service = new LoginService.LoginServiceClient();
            string json = JsonSerializer.Serialize(this);
            string jsonrespuesta = service.EditUser(json);
            service.Close();
            RM = JsonSerializer.Deserialize<RegistroMensaje>(jsonrespuesta);

            return RM;
        }

        public static List<Usuario> GetAllUsers()
        {
            AdminService.AdministradorClient service = new AdminService.AdministradorClient();
            string json = service.GetAllUsers();
            service.Close();

            List<Usuario> ListUsers = JsonSerializer.Deserialize<List<Usuario>>(json);

            return ListUsers;

        }

        public RegistroMensaje EliminarUsuario()
        {
            RegistroMensaje RM = new RegistroMensaje();

            AdminService.AdministradorClient Service = new AdminService.AdministradorClient();

            string json = Service.EliminarUsuario(this.userid);
            Service.Close();

            RM = JsonSerializer.Deserialize<RegistroMensaje>(json);

            return RM;
        }


    }
}
