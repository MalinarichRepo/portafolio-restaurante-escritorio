﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;


namespace Negocio.Clases
{
    public class UsernameLogin
    {
        public string Email { get; set; }
        public string Contrasenia { get; set; }

        public LoginRespuesta Loguear()
        {
            string UserJson = JsonSerializer.Serialize(this);

            Negocio.LoginService.LoginServiceClient LoginService = new Negocio.LoginService.LoginServiceClient();

            string JsonRespuesta = LoginService.Login(UserJson);

            LoginService.Close();

            LoginRespuesta Respuesta = JsonSerializer.Deserialize<LoginRespuesta>(JsonRespuesta);

            return Respuesta;
        }

    }
}
