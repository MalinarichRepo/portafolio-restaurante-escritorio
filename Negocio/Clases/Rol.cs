﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Clases
{
    public class Rol
    {
        public string RolID { get; set; }
        public string Descripcion { get; set; }
    }
}
