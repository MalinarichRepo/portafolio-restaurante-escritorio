﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Clases
{
    public class RecordUtilDiaria
    {
        public string ID { get; set; }
        public DateTime Fecha { get; set; }
        public decimal Gasto { get; set; }
        public decimal Ingreso { get; set; }
        public decimal Utilidad { get; set; }
        public decimal Clientes_Atendido { get; set; }
        public decimal Platos_Consumidos { get; set; }
        public decimal Media_Tiempo_Atencion_HR { get; set; }

        public static List<RecordUtilDiaria> GetAllRecords()
        {
            FinanzasService.FinanzasClient finanzascliente = new FinanzasService.FinanzasClient();
            string json = finanzascliente.GetAllRecordDiarios();

            finanzascliente.Close();

            List<RecordUtilDiaria> List = JsonSerializer.Deserialize < List<RecordUtilDiaria>>(json);

            return List;
        }
    }
}
