﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Negocio.Clases
{
    public class LoginRespuesta
    {
        public bool Estado { get; set; }
        public string Mensaje { get; set; }
        public string RolID { get; set; }
        public string UserID { get; set; }
    }
}
