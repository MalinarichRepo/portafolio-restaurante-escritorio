﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Clases
{
    public class Mesa
    {
        public string Mesaid { get; set; }
        public bool Estado { get; set; }


        public static List<Mesa> GetAllMesas()
        {
            AdminService.AdministradorClient Service = new AdminService.AdministradorClient();

            string json = Service.GetAllMesas();
            Service.Close();

            List<Mesa> ListMesas = JsonSerializer.Deserialize<List<Mesa>>(json);

            return ListMesas;
        }

        public static RegistroMensaje AgregarMesa()
        {
            AdminService.AdministradorClient Service = new AdminService.AdministradorClient();

            string json = Service.AgregarMesa();
            Service.Close();

            RegistroMensaje RM = JsonSerializer.Deserialize<RegistroMensaje>(json);

            return RM;
        }

        public static RegistroMensaje EliminarMesa()
        {
            AdminService.AdministradorClient Service = new AdminService.AdministradorClient();

            string json = Service.EliminarMesa();
            Service.Close();

            RegistroMensaje RM = JsonSerializer.Deserialize<RegistroMensaje>(json);

            return RM;
        }



    }

    



}
