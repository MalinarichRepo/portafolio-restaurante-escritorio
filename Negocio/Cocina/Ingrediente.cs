﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Cocina
{
    public class Ingrediente
    {
        public string IngredienteID { get; set; }
        public string Descripcion { get; set; }
        public string Medida { get; set; }



        public static List<Ingrediente> GetAllIngredientes()
        {
            CocinaService.CocinaClient service = new CocinaService.CocinaClient();

            string json = service.GetAllIngredientes();
            service.Close();

            List<Ingrediente> ListIngredientes = JsonSerializer.Deserialize<List<Ingrediente>>(json);

            return ListIngredientes;
        }
    }
}
