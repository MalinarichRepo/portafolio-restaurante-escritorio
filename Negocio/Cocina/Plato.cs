﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Windows.Media.Imaging;
using System.IO;

namespace Negocio.Cocina
{
    public class Plato
    {
        public string ID { get; set; }
        public string Nombre_Plato { get; set; }
        public string Descripcion_Plato { get; set; }
        public int Precio { get; set; }
        public byte[] Foto { get; set; }
        public BitmapImage FotoWPF { get; set; }
        public int Cantidad { get; set; }


        public Negocio.Clases.LoginRespuesta RegistrarPlato()
        {
            CocinaService.CocinaClient CocinaService = new CocinaService.CocinaClient();

            string json = JsonSerializer.Serialize(this);

            string jsonrespuesta = CocinaService.RegistrarPlato(json);

            Negocio.Clases.LoginRespuesta LG = JsonSerializer.Deserialize<Negocio.Clases.LoginRespuesta>(jsonrespuesta);

            return LG;
        }

        public static List<Plato> GetAllPlatos()
        {
            List<Plato> ListPlatos = new List<Plato>();

            CocinaService.CocinaClient CocinaService = new CocinaService.CocinaClient();

            string json = CocinaService.GetAllPlatos();
            

            ListPlatos = JsonSerializer.Deserialize<List<Plato>>(json);
            CocinaService.Close();
            return ListPlatos;
        }

        public Plato GetPlatoByID()
        {
            CocinaService.CocinaClient CocinaService = new CocinaService.CocinaClient();

            string json = CocinaService.GetPlatoByID(this.ID);            

            Plato p = JsonSerializer.Deserialize<Plato>(json);

            CocinaService.Close();
            return p;
        }

        public Negocio.Clases.LoginRespuesta EliminarPlatoByID()
        {
            Negocio.Clases.LoginRespuesta lr = new Clases.LoginRespuesta();

            CocinaService.CocinaClient CocinaService = new CocinaService.CocinaClient();
            

            string json = CocinaService.EliminarPlatoByID(this.ID);

            lr = JsonSerializer.Deserialize<Negocio.Clases.LoginRespuesta>(json);

            CocinaService.Close();
            return lr;
        }

        public Negocio.Clases.LoginRespuesta EditarPlato()
        {
            Negocio.Clases.LoginRespuesta lr = new Clases.LoginRespuesta();

            CocinaService.CocinaClient CocinaService = new CocinaService.CocinaClient();

            string platoSTR = JsonSerializer.Serialize(this);

            string json = CocinaService.EditarPlato(platoSTR);

            lr = JsonSerializer.Deserialize<Negocio.Clases.LoginRespuesta>(json);

            CocinaService.Close();
            return lr;
        }

    }
}
