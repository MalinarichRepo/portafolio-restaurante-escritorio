﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Cocina
{
    public class RelacionPlatoIngrediente
    {
        public string PlatoID { get; set; }
        public string IngredienteID { get; set; }
        public string CantidadConsumo { get; set; }
        public string Descripcion { get; set; }
        public string Medida { get; set; }


        public Negocio.Clases.LoginRespuesta AgregarRPI()
        {
            CocinaService.CocinaClient service = new CocinaService.CocinaClient();

            string json = JsonSerializer.Serialize(this);

            string jsonrespuesta = service.AgregarRPI(json);
            service.Close();
            Negocio.Clases.LoginRespuesta LR = JsonSerializer.Deserialize<Negocio.Clases.LoginRespuesta>(jsonrespuesta);

            return LR;

        }

        public static List<RelacionPlatoIngrediente> GetAllIngredientesByPlatoID(string platoID)
        {
            CocinaService.CocinaClient service = new CocinaService.CocinaClient();

            string jsonrespuesta = service.GetAllIngredienteByPlatoID(platoID);
            service.Close();
            List<RelacionPlatoIngrediente> ListRPI = JsonSerializer.Deserialize<List<RelacionPlatoIngrediente>>(jsonrespuesta);

            return ListRPI;
        }


        public Negocio.Clases.LoginRespuesta EliminarRPI()
        {
            CocinaService.CocinaClient service = new CocinaService.CocinaClient();

            string json = JsonSerializer.Serialize(this);

            string jsonrespuesta = service.EliminarRPI(json);
            service.Close();
            Negocio.Clases.LoginRespuesta LR = JsonSerializer.Deserialize<Negocio.Clases.LoginRespuesta>(jsonrespuesta);

            return LR;

        }

    }
}
