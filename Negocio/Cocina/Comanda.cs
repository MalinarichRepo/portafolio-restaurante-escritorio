﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Negocio.Cocina
{
    public class Comanda
    {
        public string ID { get; set; }
        public string Hora_Emision { get; set; }
        public int Estado { get; set; }
        public string MesaID { get; set; }
        public string Comentario { get; set; }
        public List<Plato> Platos { get; set; }

        public Negocio.Clases.LoginRespuesta ActualizarComanda()
        {
            Negocio.Clases.LoginRespuesta LR = new Clases.LoginRespuesta();

            CocinaService.CocinaClient service = new CocinaService.CocinaClient();

            string jsonrespuesta = service.ActualizarComanda(this.ID);

            service.Close();

            LR = JsonSerializer.Deserialize<Negocio.Clases.LoginRespuesta>(jsonrespuesta);

            return LR;
        }


    }
}
