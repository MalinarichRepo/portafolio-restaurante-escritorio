﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Finanzas
{
    public class Medida
    {
        public string Medidaid { get; set; }
        public string Medida_desc { get; set; }


        public static List<Medida> GetAllMedidas()
        {
            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();
            string json = service.GetAllMedidas();
            service.Close();

            return JsonSerializer.Deserialize<List<Medida>>(json);

        }


    }
}