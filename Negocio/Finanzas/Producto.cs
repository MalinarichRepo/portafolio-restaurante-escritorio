﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Negocio.Finanzas
{
    public class Producto
    {
        public string IDSelected { get; set; }
        public int Posicion { get; set; }
        public List<Cocina.Ingrediente> Opciones { get; set; }
        public string Cantidad { get; set; }
        public string MedidaActual { get; set; }

        public string Descripcion { get; set; }
        public string MedidaID { get; set; }

        public Producto()
        {
            IDSelected = string.Empty;
            Opciones = Cocina.Ingrediente.GetAllIngredientes();
            Cantidad = string.Empty;
            MedidaActual = "Sin Medida";
        }

        public bool RegistrarProducto()
        {
            string json = JsonSerializer.Serialize(this);

            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();

            bool estado = service.RegistrarProducto(json);
            service.Close();

            return estado;
        }

    }

    
}
