﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Negocio.Finanzas
{
    public class Factura
    {
        public string ID { get; set; }
        public string Fecha_Emision { get; set; }
        public string Tipo_Mercaderia { get; set; }
        public string Valor_Neto { get; set; }
        public string Valor_Bruto { get; set; }
        public string Rut_Proveedor { get; set; }
        public bool Estado { get; set; }
        public List<Producto> Pedido { get; set; }


        public Factura()
        {
            ID = string.Empty;
            Fecha_Emision = string.Empty;
            Tipo_Mercaderia = string.Empty;
            Valor_Neto = string.Empty;
            Valor_Bruto = string.Empty;
            Rut_Proveedor = string.Empty;
            Pedido = new List<Producto>();
        }

        public bool RegistrarFactura()
        {
            string json = JsonSerializer.Serialize(this);

            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();
            bool respuesta = service.RegistrarFactura(json);

            return respuesta;
        }

        public static List<Factura> GetAllFacturas()
        {
            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();

            string jsonrespuesta = service.GetAllFacturas();
            service.Close();
            return JsonSerializer.Deserialize<List<Factura>>(jsonrespuesta);
        }

        public bool ActualizarFactura()
        {
            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();

            
            bool estado = service.ActualizarFactura(this.ID);
            service.Close();

            return estado;
        }

    }
}
