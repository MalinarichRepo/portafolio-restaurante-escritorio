﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Finanzas
{
    public class DetalleFactura
    {
        public string Idproducto { get; set; }
        public string Descripcion { get; set; }
        public string Medida_desc { get; set; }
        public string Cantidad { get; set; }


        public static List<DetalleFactura> GetDetallesByIDFactura(string idfactura)
        {
            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();

            string json = service.GetAllDetallesByFacturaID(idfactura);
            service.Close();

            List<DetalleFactura> list = JsonSerializer.Deserialize<List<DetalleFactura>>(json);


            return list;
        }



    }
}
