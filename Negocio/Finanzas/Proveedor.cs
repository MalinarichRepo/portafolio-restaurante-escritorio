﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace Negocio.Finanzas
{
    public class Proveedor
    {
        public string Rut { get; set; }
        public string Nombre { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Direccion { get; set; }


        public Clases.RegistroMensaje RegistrarProveedor()
        {
            Clases.RegistroMensaje RM = new Clases.RegistroMensaje();

            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();

            string json = JsonSerializer.Serialize(this);

            string jsonrespuesta = service.RegistrarProveedor(json);

            RM = JsonSerializer.Deserialize<Clases.RegistroMensaje>(jsonrespuesta);

            return RM;
        }

        public static List<Proveedor> GetAllProveedores()
        {
            List<Proveedor> ListProveedores = new List<Proveedor>();
            FinanzasService.FinanzasClient service = new FinanzasService.FinanzasClient();

            string jsonrespuesta = service.GetAllProveedores();
            service.Close();

            ListProveedores = JsonSerializer.Deserialize<List<Proveedor>>(jsonrespuesta);
            return ListProveedores;
        }
    }
}
