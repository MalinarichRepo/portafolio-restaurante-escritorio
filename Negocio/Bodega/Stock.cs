﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Negocio.Bodega
{
    public class Stock
    {
        public string id { get; set; }
        public string descripcion { get; set; }
        public int cantidad { get; set; }
        public string medida { get; set; }


        public static List<Stock> GetAllStock()
        {
            BodegaService.BodegaClient service = new BodegaService.BodegaClient();

            string json = service.GetAllStock();
            service.Close();

            List<Stock> ListStock = JsonSerializer.Deserialize<List<Stock>>(json);

            return ListStock;
        }
        public static bool PedirInsumos(string mensaje)
        {
            BodegaService.BodegaClient service = new BodegaService.BodegaClient();

            bool respuesta = service.PedirInsumos(mensaje);
            service.Close();
            return respuesta;
        }
    }
}
