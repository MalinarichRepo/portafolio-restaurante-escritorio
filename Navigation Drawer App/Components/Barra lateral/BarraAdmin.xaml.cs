﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Barra_lateral
{
    /// <summary>
    /// Lógica de interacción para BarraCocina.xaml
    /// </summary>
    public partial class BarraAdmin : Page
    {
        public BarraAdmin()
        {
            InitializeComponent();
        }

        public MainWindow menu = null;

        private void ListViewItem_MouseEnter(object sender, MouseEventArgs e)
        {
            // Set tooltip visibility

            if (Tg_Btn.IsChecked == true)
            {
                tt_perfil.Visibility = Visibility.Collapsed;
                tt_pedidos.Visibility = Visibility.Collapsed;
                tt_recetas.Visibility = Visibility.Collapsed;
                tt_cerrarsesion.Visibility = Visibility.Collapsed;
            }
            else
            {
                tt_perfil.Visibility = Visibility.Visible;
                tt_pedidos.Visibility = Visibility.Visible;
                tt_recetas.Visibility = Visibility.Visible;
                tt_cerrarsesion.Visibility = Visibility.Visible;
            }
        }

        

        private void BG_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Tg_Btn.IsChecked = false;
        }

        private void btnPerfil1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Perfil(menu.userid);
        }
        /*COCINA*/
        private void StackPanel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Pedidos();
        }

        private void StackPanel_MouseLeftButtonDown_1(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Recetas();
        }
        

        /*CERRA SESION*/
        private void StackPanel_MouseLeftButtonDown_2(object sender, MouseButtonEventArgs e)
        {
            Ventanas.Login Login = new Ventanas.Login();
            Login.Show();

            menu.Close();
        }
        /*BODEGA*/
        private void StackPanel_MouseLeftButtonDown_3(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Bodega.Stock();
        }

        private void StackPanel_MouseLeftButtonDown_4(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Bodega.Movimientos();
        }
        /*Finanzas*/
        private void StackPanel_MouseLeftButtonDown_5(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Finanzas.Ganancias();
        }

        private void StackPanel_MouseLeftButtonDown_6(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Finanzas.Factura(menu);
        }
        /*AGREGARUSUARIO*/
        private void StackPanel_MouseLeftButtonDown_7(object sender, MouseButtonEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Admin.RegistrarUsuario();
        }
    }
}
