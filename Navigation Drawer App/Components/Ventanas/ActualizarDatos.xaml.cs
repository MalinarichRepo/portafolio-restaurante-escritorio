﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para ActualizarDatos.xaml
    /// </summary>
    public partial class ActualizarDatos : Window
    {
        public ActualizarDatos()
        {
            InitializeComponent();
        }
        string correo = string.Empty;
        Paginas.Perfil Perfil = null;
        public ActualizarDatos(string rut,string nombre,string apellido,string email,Paginas.Perfil perfil)
        {
            InitializeComponent();

            txtRut.Text = rut;
            txtNombre.Text = nombre;
            txtApellido.Text = apellido;
            correo = email;
            Perfil = perfil;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            string Rut = txtRut.Text.Replace(" ", "");
            string Nombre = txtNombre.Text.Replace(" ", "");
            string Apellido = txtApellido.Text.Replace(" ", "");

            if (Rut.Equals("") || Nombre.Equals("") || Apellido.Equals(""))
            {
                MessageBox.Show("Complete todos los campos antes de editar.");
            }
            else
            {
                Negocio.Clases.Usuario u = new Negocio.Clases.Usuario();

                u.Rut = Rut;
                u.Nombre = Nombre;
                u.Apellido = Apellido;
                u.Email = correo;

                Negocio.Clases.RegistroMensaje RM = u.EditarUser();



                MessageBox.Show(RM.Mensaje);
                if (RM.Estado)
                {
                    Perfil.txbRut.Text = Rut;
                    Perfil.txbNombre.Text = Nombre;
                    Perfil.txbApellido.Text = Apellido;

                    Close();
                }
            }
        }

        private void txtRut_KeyUp(object sender, KeyEventArgs e)
        {
            txtRut.Text = FormatearRut(txtRut.Text);

            txtRut.SelectionStart = txtRut.Text.Length;
            txtRut.SelectionLength = 0;
        }

        public static string FormatearRut(string rut)
        {
            string rutFormateado = string.Empty;

            if (rut.Length == 0)
            {
                rutFormateado = "";
            }
            else
            {
                string rutTemporal;
                string dv;
                Int64 rutNumerico;

                rut = rut.Replace("-", "").Replace(".", "");

                if (rut.Length == 1)
                {
                    rutFormateado = rut;
                }
                else
                {
                    rutTemporal = rut.Substring(0, rut.Length - 1);
                    dv = rut.Substring(rut.Length - 1, 1);

                    //aqui convierto a un numero el RUT si ocurre un error lo deja en CERO
                    if (!Int64.TryParse(rutTemporal, out rutNumerico))
                    {
                        rutNumerico = 0;
                    }

                    //este comando es el que formatea con los separadores de miles
                    rutFormateado = rutNumerico.ToString("N0");

                    if (rutFormateado.Equals("0"))
                    {
                        rutFormateado = string.Empty;
                    }
                    else
                    {
                        //si no hubo problemas con el formateo agrego el DV a la salida
                        rutFormateado += "-" + dv;

                        //y hago este replace por si el servidor tuviese configuracion anglosajona y reemplazo las comas por puntos
                        rutFormateado = rutFormateado.Replace(",", ".");
                    }
                }
            }

            return rutFormateado;
        }
    }
}
