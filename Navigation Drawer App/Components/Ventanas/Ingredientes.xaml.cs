﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para Ingredientes.xaml
    /// </summary>
    public partial class Ingredientes : Window
    {
        List<Negocio.Cocina.Ingrediente> Lista = Negocio.Cocina.Ingrediente.GetAllIngredientes();
        List<Negocio.Cocina.RelacionPlatoIngrediente> ListaGrid = new List<Negocio.Cocina.RelacionPlatoIngrediente>();


        string IDplato = string.Empty;
        public Ingredientes(string nombreplato,string ID)
        {
            InitializeComponent();
            txbTitulo.Text = nombreplato;
            IDplato = ID;
            Cargar();
        }

        private void Cargar()
        {
            cbIngrediente.ItemsSource = Lista;
            cbIngrediente.DisplayMemberPath = "Descripcion";
            cbIngrediente.SelectedValuePath = "IngredienteID";
            cbIngrediente.SelectedIndex = 0;
            ListaGrid = Negocio.Cocina.RelacionPlatoIngrediente.GetAllIngredientesByPlatoID(IDplato);
            dgIngredientesPlato.ItemsSource = ListaGrid;
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void cbIngrediente_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            foreach (Negocio.Cocina.Ingrediente I in Lista)
            {
                if (I.IngredienteID.Equals(cbIngrediente.SelectedValue.ToString()))
                {
                    txbTipoMedida.Text = I.Medida;
                    break;
                }
            }
        }

        private void txtMedida_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            if (txtMedida.Equals(""))
            {
                MessageBox.Show("Llene el campo restante.");
            }
            else
            {
                Negocio.Cocina.RelacionPlatoIngrediente RPI = new Negocio.Cocina.RelacionPlatoIngrediente();

                RPI.PlatoID = IDplato;
                RPI.IngredienteID = cbIngrediente.SelectedValue.ToString();
                RPI.CantidadConsumo = txtMedida.Text;

                Negocio.Clases.LoginRespuesta mensaje = RPI.AgregarRPI();

                MessageBox.Show(mensaje.Mensaje);
                Cargar();
            }
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Cocina.RelacionPlatoIngrediente RPI = dgIngredientesPlato.SelectedItem as Negocio.Cocina.RelacionPlatoIngrediente;

            if (RPI != null)
            {
                if (RPI.IngredienteID != "")
                {
                    RPI.PlatoID = IDplato;
                    Negocio.Clases.LoginRespuesta respuesta = RPI.EliminarRPI();

                    if (respuesta.Estado)
                    {
                        MessageBox.Show("Se ha eliminado el ingrediente seleccionado");
                        Cargar();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al intentar eliminar");
                    }


                }
                else
                {
                    MessageBox.Show("Seleccione una fila");
                }

            }
            else
            {
                MessageBox.Show("Seleccione una fila");
            }
        }
    }
}
