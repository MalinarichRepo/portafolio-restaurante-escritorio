﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para AgregarFactura.xaml
    /// </summary>
    public partial class AgregarFactura : Window
    {
        public AgregarFactura()
        {
            InitializeComponent();
            CargarProveedores();

            dpFechaEmision.DisplayDateEnd = DateTime.Today;
            List<Negocio.Finanzas.Producto> ListProducto = new List<Negocio.Finanzas.Producto>();
            icProductos.ItemsSource = ListProducto;

        }

        private void CargarProveedores()
        {
            cbProveedores.ItemsSource = Negocio.Finanzas.Proveedor.GetAllProveedores();
            cbProveedores.DisplayMemberPath = "Nombre";
            cbProveedores.SelectedValuePath = "Rut";
            cbProveedores.SelectedIndex = 0;
        }
        
        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        int Contador = 0;
        private void btnAgregarProducto_Click(object sender, RoutedEventArgs e)
        {
            var Lista = icProductos.ItemsSource;
            Negocio.Finanzas.Producto producto = new Negocio.Finanzas.Producto();

            

            List<Negocio.Finanzas.Producto> ProductList = new List<Negocio.Finanzas.Producto>();



            foreach (Negocio.Finanzas.Producto p in Lista)
            {
                ProductList.Add(p);
            }

            producto.Posicion = Contador;
            ProductList.Add(producto);


            icProductos.ItemsSource = ProductList;
            Contador += 1;
        }

        private void btnEliminarProducto_Click(object sender, RoutedEventArgs e)
        {
            var Lista = icProductos.ItemsSource;
            List<Negocio.Finanzas.Producto> ProductList = new List<Negocio.Finanzas.Producto>();
            foreach (Negocio.Finanzas.Producto p in Lista)
            {
                ProductList.Add(p);
            }

            try
            {
                ProductList.RemoveAt(ProductList.Count - 1);
            }
            catch (Exception)
            {

            }

            icProductos.ItemsSource = ProductList;
            Contador -= 1;
        }

        private void btnRegistrar_Click(object sender, RoutedEventArgs e)
        {
            var ListaFactura = icProductos.ItemsSource;

            

            Negocio.Finanzas.Factura factura = new Negocio.Finanzas.Factura();

            factura.Fecha_Emision = dpFechaEmision.Text;
            factura.Tipo_Mercaderia = txtTipoMercaderia.Text;
            factura.Valor_Neto = txtValorNeto.Text;
            factura.Valor_Bruto = txtValorBruto.Text;
            factura.Rut_Proveedor = cbProveedores.SelectedValue.ToString();

            List<Negocio.Finanzas.Producto> listaprod = new List<Negocio.Finanzas.Producto>();

            foreach (Negocio.Finanzas.Producto item in ListaFactura)
            {
                listaprod.Add(item);
            }

            factura.Pedido = listaprod;

            bool respeusta = factura.RegistrarFactura();

            if (respeusta)
            {
                MessageBox.Show("Factura agregada exitosamente");
            }
            else
            {
                MessageBox.Show("Hubo un error al intentar registrar la factura");
            }
            Close();
        }
        
    }
}
