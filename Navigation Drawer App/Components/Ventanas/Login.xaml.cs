﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para Login.xaml
    /// </summary>
    public partial class Login : Window
    {

        
        public Login()
        {
            InitializeComponent();

            LoginService.LoginServiceClient LS = new LoginService.LoginServiceClient();
            try
            {
                bool status = LS.ConnectionStatus();
            }
            catch (Exception)
            {
                MessageBox.Show("Sin conexion");
                Close();
            }

            LS.Close();
        }

        private void TextBlock_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        /*Tipos de Roles
         0 = Admin
         1 = Cocina
         2 = Bodega
         3 = Finanzas*/
        int[] Roles = { 0, 1, 2, 3 };

        private void Menu()
        {
            Negocio.Clases.UsernameLogin user = new Negocio.Clases.UsernameLogin();

            user.Email = txtEmail.Text;
            user.Contrasenia = txtPassword.Password.ToString();

            Negocio.Clases.LoginRespuesta respuesta = new Negocio.Clases.LoginRespuesta(); 

            respuesta = user.Loguear();

            if (respuesta.Estado)
            {
                MainWindow mw = new MainWindow(Int32.Parse(respuesta.RolID),respuesta.UserID);
                mw.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Email o contraseña incorrectos");
            }

            
            
        }

        private void PasswordBox_KeyDown(object sender, KeyEventArgs e)
            {
            if (e.Key == Key.Enter)
            {
                Menu();
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Menu();
        }

        private void btnResetPassword_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Clases.Usuario user = new Negocio.Clases.Usuario();

            Negocio.Clases.RegistroMensaje msg = new Negocio.Clases.RegistroMensaje();

            user.Email = txtEmail.Text;

            msg = user.ResetPassword();

            MessageBox.Show(msg.Mensaje);
        }
    }
}
