﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para CambiarContrasenia.xaml
    /// </summary>
    public partial class CambiarContrasenia : Window
    {
        string iduser = string.Empty;
        public CambiarContrasenia()
        {
            InitializeComponent();
        }

        public CambiarContrasenia (string userid)
        {
            InitializeComponent();
            iduser = userid;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {

            /* Verificar campos de la nueva contraseña */
            string Actual = txtActualPassword.Password.Replace(" ", "");
            string New = txtNewPassword.Password.Replace(" ", "");
            string Repeat = txtRepeatPassword.Password.Replace(" ", "");

            if (Actual.Equals("") || New.Equals("") || Repeat.Equals(""))
            {
                MessageBox.Show("Complete todos los campos antes de registrar.");
            }
            else
            {
                if (New == Repeat)
                {
                    Negocio.Clases.Usuario u = new Negocio.Clases.Usuario();
                    Negocio.Clases.RegistroMensaje RM = u.ChangePassword(iduser, New);




                    MessageBox.Show(RM.Mensaje);
                    if (RM.Estado)
                    {
                        this.Close();
                    }

                }
                else
                {
                    MessageBox.Show("Las contraseñas no coinciden.");
                }
            }
            /* Cambiar la contraseña */
        }
    }
}
