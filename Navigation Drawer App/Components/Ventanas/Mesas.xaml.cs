﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para Mesas.xaml
    /// </summary>
    public partial class Mesas : Window
    {
        public Mesas()
        {
            InitializeComponent();

            CargarGrid();
        }

        private void CargarGrid()
        {
            dgMesas.ItemsSource = Negocio.Clases.Mesa.GetAllMesas();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAgregar_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Clases.RegistroMensaje RM = Negocio.Clases.Mesa.AgregarMesa();

            MessageBox.Show(RM.Mensaje);

            CargarGrid();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Clases.RegistroMensaje RM = Negocio.Clases.Mesa.EliminarMesa();

            MessageBox.Show(RM.Mensaje);

            CargarGrid();
        }
    }
}
