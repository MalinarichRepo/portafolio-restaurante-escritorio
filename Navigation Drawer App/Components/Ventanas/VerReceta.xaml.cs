﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para VerReceta.xaml
    /// </summary>
    public partial class VerReceta : Window
    {
        public VerReceta(string idplato)
        {
            InitializeComponent();

            Negocio.Cocina.Plato plato = new Negocio.Cocina.Plato();
            plato.ID = idplato;
            plato = plato.GetPlatoByID();
            plato.FotoWPF = LoadImage(plato.Foto);

            txtNombrePlato.Text = plato.Nombre_Plato;
            txtDescripcion.Text = plato.Descripcion_Plato;
            txtPrecio.Text = plato.Precio.ToString();
            imgSelectedImage.Source = plato.FotoWPF;
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private void txtPrecio_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {

        }
    }
}
