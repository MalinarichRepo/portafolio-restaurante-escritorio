﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para Usuarios.xaml
    /// </summary>
    public partial class Usuarios : Window
    {
        public Usuarios()
        {
            InitializeComponent();
            CargarGrid();
        }

        private void CargarGrid()
        {
            dgUsuarios.ItemsSource = Negocio.Clases.Usuario.GetAllUsers();
        }

        private void btnCerrar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Clases.Usuario user = dgUsuarios.SelectedItem as Negocio.Clases.Usuario;

            if (user != null)
            {
                if (user.userid != "")
                {
                    Negocio.Clases.RegistroMensaje respuesta = user.EliminarUsuario();

                    if (respuesta.Estado)
                    {
                        MessageBox.Show("Se ha eliminado el usuario seleccionado");
                        CargarGrid();
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error al intentar eliminar");
                    }


                }
                else
                {
                    MessageBox.Show("Seleccione una fila");
                }

            }
            else
            {
                MessageBox.Show("Seleccione una fila");
            }
        }
    }
}
