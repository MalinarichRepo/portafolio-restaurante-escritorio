﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para AgregarProducto.xaml
    /// </summary>
    public partial class AgregarProducto : Window
    {
        public AgregarProducto()
        {
            InitializeComponent();
            cbMedida.ItemsSource = Negocio.Finanzas.Medida.GetAllMedidas();
            cbMedida.DisplayMemberPath = "Medida_desc";
            cbMedida.SelectedValuePath = "Medidaid";
            cbMedida.SelectedIndex = 0;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (txtDescripcion.Text.Equals(""))
            {
                MessageBox.Show("Llene el campo solicitado");
            }
            else
            {
                Negocio.Finanzas.Producto producto = new Negocio.Finanzas.Producto();

                producto.Descripcion = txtDescripcion.Text;
                producto.MedidaID = cbMedida.SelectedValue.ToString();

                bool estado = producto.RegistrarProducto();

                if (estado)
                {
                    MessageBox.Show("Producto agregado exitosamente");
                }
                else
                {
                    MessageBox.Show("hubo un error al registrar el producto");
                }
                Close();
            }
        }
    }
}
