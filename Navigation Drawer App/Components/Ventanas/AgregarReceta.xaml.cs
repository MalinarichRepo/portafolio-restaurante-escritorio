﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MessageBox = System.Windows.MessageBox;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para AgregarReceta.xaml
    /// </summary>
    public partial class AgregarReceta : Window
    {
        public AgregarReceta()
        {
            InitializeComponent();
        }

        private void btnSeleccionarImagen_Click(object sender, RoutedEventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog() { Filter="JPEG|*.jpg",ValidateNames=true,Multiselect=false})
            {
                if (ofd.ShowDialog()==System.Windows.Forms.DialogResult.OK)
                {
                    string filename = ofd.FileName;
                    imgSelectedImage.Source = new BitmapImage(new Uri(filename));
                }
            }
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnAceptar_Click(object sender, RoutedEventArgs e)
        {
            if (txtNombrePlato.Text.Equals("") || txtDescripcion.Text.Equals("") || txtPrecio.Text.Equals(""))
            {
                MessageBox.Show("Complete todos los campos antes de registrar.");
            }
            else
            {
                Negocio.Cocina.Plato plato = new Negocio.Cocina.Plato();

                plato.Nombre_Plato = txtNombrePlato.Text;
                plato.Descripcion_Plato = txtDescripcion.Text;
                plato.Precio = Convert.ToInt32(txtPrecio.Text);
                try
                {
                    plato.Foto = getJPGFromImageControl(imgSelectedImage.Source as BitmapImage);
                    Negocio.Clases.LoginRespuesta respuesta = plato.RegistrarPlato();

                    if (respuesta.Estado)
                    {
                        MessageBox.Show(respuesta.Mensaje);
                        Close();
                    }
                    else
                    {
                        MessageBox.Show(respuesta.Mensaje);
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Inserte una foto jpg valida");
                }



            }           

        }

        public byte[] getJPGFromImageControl(BitmapImage imageC)
        {
            MemoryStream memStream = new MemoryStream();
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(imageC));
            encoder.Save(memStream);
            return memStream.ToArray();
        }

        private void txtPrecio_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }
    }
}
