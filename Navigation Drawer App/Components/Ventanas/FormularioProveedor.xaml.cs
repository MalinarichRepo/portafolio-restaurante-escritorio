﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Ventanas
{
    /// <summary>
    /// Lógica de interacción para FormularioProveedor.xaml
    /// </summary>
    public partial class FormularioProveedor : Window
    {
        public FormularioProveedor()
        {
            InitializeComponent();
        }

        private void btnRegistrar_Click(object sender, RoutedEventArgs e)
        {
            string Rut = txtRut.Text.Replace(" ", "");
            string Nombre = txtNombre.Text;
            string Telefono1 = txtTelefono1.Text.Replace(" ", "");
            string Telefono2 = txtTelefono2.Text.Replace(" ", "");
            string Direccion = txtDireccion.Text;

            if (Rut.Equals("") || Nombre.Equals("") || Telefono1.Equals("") || Telefono2.Equals("") || Direccion.Equals(""))
            {
                MessageBox.Show("Complete todos los campos antes de registrar.");
            }
            else
            {
                Negocio.Finanzas.Proveedor proveedor = new Negocio.Finanzas.Proveedor();
                proveedor.Rut = Rut;
                proveedor.Nombre = Nombre;
                proveedor.Telefono1 = Telefono1;
                proveedor.Telefono2 = Telefono2;
                proveedor.Direccion = Direccion;

                Negocio.Clases.RegistroMensaje RM = proveedor.RegistrarProveedor();

                MessageBox.Show(RM.Mensaje);
                if (RM.Estado)
                {
                    txtRut.Text = "";
                    txtNombre.Text = "";
                    txtTelefono1.Text = "";
                    txtTelefono2.Text = "";
                    txtDireccion.Text = "";
                    Close();
                }
            }
        }

        private void txtRut_KeyUp(object sender, KeyEventArgs e)
        {
            txtRut.Text = FormatearRut(txtRut.Text);

            txtRut.SelectionStart = txtRut.Text.Length;
            txtRut.SelectionLength = 0;
        }

        public static string FormatearRut(string rut)
        {
            string rutFormateado = string.Empty;

            if (rut.Length == 0)
            {
                rutFormateado = "";
            }
            else
            {
                string rutTemporal;
                string dv;
                Int64 rutNumerico;

                rut = rut.Replace("-", "").Replace(".", "");

                if (rut.Length == 1)
                {
                    rutFormateado = rut;
                }
                else
                {
                    rutTemporal = rut.Substring(0, rut.Length - 1);
                    dv = rut.Substring(rut.Length - 1, 1);

                    //aqui convierto a un numero el RUT si ocurre un error lo deja en CERO
                    if (!Int64.TryParse(rutTemporal, out rutNumerico))
                    {
                        rutNumerico = 0;
                    }

                    //este comando es el que formatea con los separadores de miles
                    rutFormateado = rutNumerico.ToString("N0");

                    if (rutFormateado.Equals("0"))
                    {
                        rutFormateado = string.Empty;
                    }
                    else
                    {
                        //si no hubo problemas con el formateo agrego el DV a la salida
                        rutFormateado += "-" + dv;

                        //y hago este replace por si el servidor tuviese configuracion anglosajona y reemplazo las comas por puntos
                        rutFormateado = rutFormateado.Replace(",", ".");
                    }
                }
            }

            return rutFormateado;
        }

        private void btnCancelar_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
