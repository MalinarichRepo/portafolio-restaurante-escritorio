﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Navigation_Drawer_App.Components.Paginas
{
    /// <summary>
    /// Lógica de interacción para Perfil.xaml
    /// </summary>
    public partial class Perfil : Page
    {

        string usid = string.Empty;
        public Perfil()
        {
            InitializeComponent();
        }
        public Perfil(string userid)
        {
            InitializeComponent();
            usid = userid;

            LoginService.LoginServiceClient LS = new LoginService.LoginServiceClient();

            string json = LS.GetPerfil(userid);

            LS.Close();

            Negocio.Clases.Usuario user = JsonSerializer.Deserialize<Negocio.Clases.Usuario>(json);

            txbRut.Text = user.Rut;
            txbNombre.Text = user.Nombre;
            txbApellido.Text = user.Apellido;
            txbEmail.Text = user.Email;
            txbRol.Text = user.roldescripcion;
            txbFecha.Text = user.Fecha;
        }

        private void btnChangePassword_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.CambiarContrasenia a = new Ventanas.CambiarContrasenia(usid);
            a.ShowDialog();
        }

        private void btnActualizarDatos_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.ActualizarDatos AD = new Ventanas.ActualizarDatos(txbRut.Text,txbNombre.Text,txbApellido.Text,txbEmail.Text,this);
            AD.ShowDialog();
        }
    }
}
