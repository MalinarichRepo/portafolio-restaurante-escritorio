﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Paginas
{
    /// <summary>
    /// Lógica de interacción para Recetas.xaml
    /// </summary>
    public partial class Recetas : Page
    {



        public Recetas()
        {
            InitializeComponent();
            ActulizarListaPlatos();
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private void ActulizarListaPlatos()
        {
            List<Negocio.Cocina.Plato> listplatos = Negocio.Cocina.Plato.GetAllPlatos();

            foreach (Negocio.Cocina.Plato p in listplatos)
            {
                var ImageMS = new MemoryStream(p.Foto);

                p.FotoWPF = LoadImage(p.Foto);

            }

            RecetasIC.ItemsSource = listplatos;
        }

        private void btnAgregarReceta_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.AgregarReceta AgregarRecetas = new Ventanas.AgregarReceta();
            AgregarRecetas.ShowDialog();
            ActulizarListaPlatos();
        }

        private void btnVer_Click(object sender, RoutedEventArgs e)
        {
            string idplato = ((Button)sender).Tag.ToString();

            Ventanas.VerReceta verreceta = new Ventanas.VerReceta(idplato);

            verreceta.ShowDialog();
        }

        private void btnEliminar_Click(object sender, RoutedEventArgs e)
        {
            string idplato = ((Button)sender).Tag.ToString();

            if (MessageBox.Show("Quiere eliminar este plato?","Eliminar platillo",MessageBoxButton.YesNo)==MessageBoxResult.Yes)
            {
                Negocio.Cocina.Plato p = new Negocio.Cocina.Plato();
                p.ID = idplato;
                Negocio.Clases.LoginRespuesta lr = p.EliminarPlatoByID();

                MessageBox.Show(lr.Mensaje);
                ActulizarListaPlatos();
            }
        }

        private void btnBuscar_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void txtBuscar_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtBuscar.Text != "")
            {
                List<Negocio.Cocina.Plato> lp = new List<Negocio.Cocina.Plato>();
                foreach (Negocio.Cocina.Plato p in RecetasIC.ItemsSource)
                {
                    string lowername = p.Nombre_Plato.ToLower();
                    if (lowername.Contains(txtBuscar.Text.ToLower()))
                    {
                        lp.Add(p);
                    }
                }

                if (lp.Count == 0)
                {
                    ActulizarListaPlatos();
                }
                else
                {
                    RecetasIC.ItemsSource = lp;
                }
                
            }
            else
            {
                ActulizarListaPlatos();
            }
        }

        private void btnEditar_Click(object sender, RoutedEventArgs e)
        {
            string idplato = ((Button)sender).Tag.ToString();
            Ventanas.EditarPlato editarplato = new Ventanas.EditarPlato(idplato);
            editarplato.ShowDialog();
            ActulizarListaPlatos();
        }

        private void btnIngredientes_Click(object sender, RoutedEventArgs e)
        {
            string nombreplato = ((Button)sender).Tag.ToString();
            string ID = ((Button)sender).Content.ToString();
            Ventanas.Ingredientes ventana = new Ventanas.Ingredientes(nombreplato,ID);
            ventana.ShowDialog();
        }
    }
}
