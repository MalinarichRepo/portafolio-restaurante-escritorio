﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace Navigation_Drawer_App.Components.Paginas
{
    /// <summary>
    /// Lógica de interacción para Pedidos.xaml
    /// </summary>
    public partial class Pedidos : Page
    {

      public Pedidos()
        {
            InitializeComponent();
            GetAllComandas();
            /*
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 5000;
            timer.Elapsed += async (sender, e) => {
                GetAllComandas();
            };
            timer.Start();
            */
            DispatcherTimer disp = new DispatcherTimer();
            disp.Interval = TimeSpan.FromSeconds(5);
            disp.Tick += async (sender, e) => {
                GetAllComandas();
            };
            disp.Start();

        }



        private void ScrollViewer_PreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            /*ScrollViewer scv = (ScrollViewer)sender;
            scv.ScrollToHorizontalOffset(scv.VerticalOffset - e.Delta);
            e.Handled = true;*/
            
            ScrollViewer scrollviewer = sender as ScrollViewer;
            if (e.Delta > 0)
            {
                scrollviewer.LineLeft();
            }
            else
            {
                scrollviewer.LineRight();
            }
            e.Handled = true;
        }

        
        public void GetAllComandas()
        {
            List<Negocio.Cocina.Comanda> Listcomandas = new List<Negocio.Cocina.Comanda>();

            CocinaService.CocinaClient service = new CocinaService.CocinaClient();

            string json = service.GetAllFalseComandas();

            service.Close();

            Listcomandas = JsonSerializer.Deserialize<List<Negocio.Cocina.Comanda>>(json);

            ComandaIC.ItemsSource = Listcomandas;
        }

        private void btnAprobarComanda_Click(object sender, RoutedEventArgs e)
        {
            string idcomanda = ((Button)sender).Tag.ToString();
            Negocio.Cocina.Comanda comanda = new Negocio.Cocina.Comanda();

            comanda.ID = idcomanda;
            Negocio.Clases.LoginRespuesta LR = comanda.ActualizarComanda();

            if (LR.Estado==false)
            {
                MessageBox.Show(LR.Mensaje);
            }
            GetAllComandas();
        }
    }
}
