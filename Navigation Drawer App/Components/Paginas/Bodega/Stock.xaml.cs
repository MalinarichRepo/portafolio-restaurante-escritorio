﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Paginas.Bodega
{
    /// <summary>
    /// Lógica de interacción para Stock.xaml
    /// </summary>
    public partial class Stock : Page
    {
        public Stock()
        {
            InitializeComponent();

            ActualizarDatos();
        }

        private void ActualizarDatos()
        {
            dgStock.ItemsSource = Negocio.Bodega.Stock.GetAllStock();
        }

        private void btnPedirInsumo_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Bodega.Stock stock = dgStock.SelectedItem as Negocio.Bodega.Stock;

            if (stock!=null)
            {
                if (stock.id!="")
                {
                    bool respuesta = Negocio.Bodega.Stock.PedirInsumos("Se Solicitan insumos de " + stock.descripcion + ", quedan " + stock.cantidad.ToString()+" "+stock.medida+".");

                    if (respuesta)
                    {
                        MessageBox.Show("Se solicitaron "+stock.descripcion+" con exito.");
                    }
                    else
                    {
                        MessageBox.Show("Ocurrio un error");
                    }


                }
                else
                {
                    MessageBox.Show("Seleccione una fila");
                }
                
            }
            else
            {
                MessageBox.Show("Seleccione una fila");
            }
        }

        private void txtBuscarPorNombre_KeyUp(object sender, KeyEventArgs e)
        {
            if (txtBuscarPorNombre.Text != "")
            {
                List<Negocio.Bodega.Stock> ListStock = new List<Negocio.Bodega.Stock>();
                foreach (Negocio.Bodega.Stock s in Negocio.Bodega.Stock.GetAllStock())
                {
                    string lowername = s.descripcion.ToLower();
                    if (lowername.Contains(txtBuscarPorNombre.Text.ToLower()))
                    {
                        ListStock.Add(s);
                    }

                    if (ListStock.Count == 0)
                    {
                        ActualizarDatos();
                    }
                    else
                    {
                        dgStock.ItemsSource = ListStock;
                    }


                }

            }
            else
            {
                ActualizarDatos();
            }
        }

        


    }
}
