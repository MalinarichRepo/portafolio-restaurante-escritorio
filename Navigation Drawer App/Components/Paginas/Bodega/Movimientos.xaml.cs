﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Paginas.Bodega
{
    /// <summary>
    /// Lógica de interacción para Movimientos.xaml
    /// </summary>
    public partial class Movimientos : Page
    {
        public Movimientos()
        {
            InitializeComponent();

            CargarFalseFacturas();
        }

        private void CargarFalseFacturas()
        {
            List<Negocio.Finanzas.Factura> ListaFiltrada = new List<Negocio.Finanzas.Factura>();
            foreach (Negocio.Finanzas.Factura f in Negocio.Finanzas.Factura.GetAllFacturas())
            {
                if (f.Estado==false)
                {
                    ListaFiltrada.Add(f);
                }
            }

            dgFacturas.ItemsSource = ListaFiltrada;
        }

        private void btnRegistrarMovimiento_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Finanzas.Factura factura = dgFacturas.SelectedItem as Negocio.Finanzas.Factura;
            if (factura != null)
            {
                if (factura.ActualizarFactura())
                {
                    MessageBox.Show("Se ha hecho el movimiento correctamente");
                    CargarFalseFacturas();
                }
                else
                {
                    MessageBox.Show("hubo un error al hacer el movimiento");
                }
            }
            else
            {
                MessageBox.Show("Seleccione una fila");
            }
        }

        private void btnVerDetalle_Click(object sender, RoutedEventArgs e)
        {
            Negocio.Finanzas.Factura factura = dgFacturas.SelectedItem as Negocio.Finanzas.Factura;
            if (factura != null)
            {
                Ventanas.DetalleFactura ventana = new Ventanas.DetalleFactura(factura.ID);
                ventana.ShowDialog();
            }
            else
            {
                MessageBox.Show("Seleccione una fila");
            }
        }
    }
}
