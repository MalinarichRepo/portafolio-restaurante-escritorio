﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Paginas.Finanzas
{
    /// <summary>
    /// Lógica de interacción para AgregarFactura.xaml
    /// </summary>
    public partial class AgregarFactura : Page
    {
        private MainWindow menu;

        public AgregarFactura()
        {
            InitializeComponent();
            List<Producto> items = new List<Producto>();
            ProductosIC.ItemsSource = items;
            
        }

        public AgregarFactura(MainWindow menuIN)
        {
            menu = menuIN;
            InitializeComponent();
            List<Producto> items = new List<Producto>();
            ProductosIC.ItemsSource = items;
        }

        public class Producto
        {
            public string Nombre { get; set; }
            public int Cantidad { get; set; }
        }


        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var test = ProductosIC.ItemsSource;
            Producto prod = new Producto();

            prod.Nombre = string.Empty;
            prod.Cantidad = 0;

            List<Producto> ProductList = new List<Producto>();

            

            foreach (Producto p in test)
            {
                ProductList.Add(p);
            }
            ProductList.Add(prod);


            ProductosIC.ItemsSource = ProductList;


        }

        private void deletebtn_Click(object sender, RoutedEventArgs e)
        {
            var test = ProductosIC.ItemsSource;
            List<Producto> ProductList = new List<Producto>();
            foreach (Producto p in test)
            {
                ProductList.Add(p);
            }

            try
            {
                ProductList.RemoveAt(ProductList.Count - 1);
            }
            catch (Exception)
            {

                ;
            }

            ProductosIC.ItemsSource = ProductList;

        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Finanzas.Factura(menu);
        }
    }
}
