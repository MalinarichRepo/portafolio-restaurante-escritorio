﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Paginas.Finanzas
{
    /// <summary>
    /// Lógica de interacción para AgregarProveedor.xaml
    /// </summary>
    public partial class AgregarProveedor : Page
    {
        private MainWindow menu;

        public AgregarProveedor()
        {
            InitializeComponent();
        }

        public AgregarProveedor(MainWindow menuIN)
        {
            menu = menuIN;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            menu.Contenido.Content = new Components.Paginas.Finanzas.Factura(menu);
        }
    }
}
