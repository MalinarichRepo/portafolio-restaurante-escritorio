﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App.Components.Paginas.Finanzas
{
    /// <summary>
    /// Lógica de interacción para Factura.xaml
    /// </summary>
    public partial class Factura : Page
    {
        private MainWindow menu;

        public Factura()
        {
            InitializeComponent();
        }

        public Factura(MainWindow framemenu)
        {
            menu = framemenu;
            InitializeComponent();
            CargarGrid();
        }

        private void CargarGrid()
        {
            dgFacturas.ItemsSource = Negocio.Finanzas.Factura.GetAllFacturas();
        }

        private void AgregarFacturabtn_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.AgregarFactura ventana = new Ventanas.AgregarFactura();
            ventana.ShowDialog();
            CargarGrid();
        }

        private void AgregarProveedorbtn_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.FormularioProveedor ventana = new Ventanas.FormularioProveedor();
            ventana.ShowDialog();
        }

        private void btnAgregarProducto_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.AgregarProducto ventana = new Ventanas.AgregarProducto();
            ventana.ShowDialog();
        }
    }
}
