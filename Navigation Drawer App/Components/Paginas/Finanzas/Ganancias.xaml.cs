﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

using iTextSharp.tool;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Windows.Forms;
using SaveFileDialog = System.Windows.Forms.SaveFileDialog;
using iTextSharp.tool.xml;

namespace Navigation_Drawer_App.Components.Paginas.Finanzas
{
    /// <summary>
    /// Lógica de interacción para Ganancias.xaml
    /// </summary>
    public partial class Ganancias : Page
    {
        public Ganancias()
        {
            InitializeComponent();

            for (int i = 1; i < 13; i++)
            {
                MesCB.Items.Add(i.ToString());
            }

            for (int i = 2000; i < 2100; i++)
            {
                AnioCB.Items.Add(i.ToString());
            }

            VerCB.Items.Add("Diarias");
            VerCB.Items.Add("Mensuales");

            dgGanancias.ItemsSource = Negocio.Clases.RecordUtilDiaria.GetAllRecords();

        }

        public class prueba
        {
            public string rut { get; set; }
            public string nombre { get; set; }
            public string apellido { get; set; }
        }

        private void btnExportToPDF_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.FileName = "Ganancias Restaurante "+DateTime.Now.ToString("dd MM yyyy")+".pdf";


            string templatehtml_text = Properties.Resources.templateExportToPDF.ToString();

            templatehtml_text = templatehtml_text.Replace("@Fecha",DateTime.Now.ToString("dd/MM/yyyy"));
            templatehtml_text = templatehtml_text.Replace("@Hora",DateTime.Now.ToString("HH:mm:ss"));

            string Tablas = string.Empty;

            foreach (Negocio.Clases.RecordUtilDiaria item in dgGanancias.ItemsSource)
            {
                Tablas += "<tr>";
                Tablas += "<td>"+item.ID+"</td>";
                Tablas += "<td>" + item.Fecha.ToString("dd/MM/yyyy") + "</td>";
                Tablas += "<td>" + item.Gasto.ToString() + "</td>";
                Tablas += "<td>" + item.Ingreso.ToString() + "</td>";
                Tablas += "<td>" + item.Utilidad.ToString() + "</td>";
                Tablas += "<td>" + item.Clientes_Atendido.ToString() + "</td>";
                Tablas += "<td>" + item.Platos_Consumidos.ToString() + "</td>";
                Tablas += "<td>" + item.Media_Tiempo_Atencion_HR.ToString() + "</td>";
                Tablas += "</tr>";
            }

            templatehtml_text = templatehtml_text.Replace("@Tablas", Tablas);

            if (save.ShowDialog()==DialogResult.OK)
            {
                using (FileStream stream = new FileStream(save.FileName, FileMode.Create))
                {
                    Document pdfDoc = new Document(PageSize.A4, 25, 25, 25, 25);

                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, stream);

                    pdfDoc.Open();

                    using (StringReader sr = new StringReader(templatehtml_text))
                    {
                        XMLWorkerHelper.GetInstance().ParseXHtml(writer, pdfDoc, sr);
                    }

                    pdfDoc.Close();

                    stream.Close();
                }


                    




            }
        }
    }
}
