﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.RegularExpressions;

namespace Navigation_Drawer_App.Components.Paginas.Admin
{
    /// <summary>
    /// Lógica de interacción para RegistrarUsuario.xaml
    /// </summary>
    public partial class RegistrarUsuario : Page
    {
        public RegistrarUsuario()
        {
            InitializeComponent();

            AdminService.AdministradorClient AS = new AdminService.AdministradorClient();
            string JsonRol = AS.GetRols();
            AS.Close();
            List<Negocio.Clases.Rol> ListRol = JsonSerializer.Deserialize<List<Negocio.Clases.Rol>>(JsonRol);

            cbRol.DisplayMemberPath = "Descripcion";
            cbRol.SelectedValuePath = "RolID";
            cbRol.ItemsSource = ListRol;
            cbRol.SelectedIndex = 0;
            
        }

        private void btnRegistrar_Click(object sender, RoutedEventArgs e)
        {
            string Rut = txtRut.Text.Replace(" ","");
            string Nombre = txtNombre.Text.Replace(" ", "");
            string Apellido = txtApellido.Text.Replace(" ", "");
            string Email = txtEmail.Text.Replace(" ", "");
            string Contrasenia = txtContrasenia.Password.Replace(" ", "");
            string RepiteContrasenia = txtRepiteContrasenia.Password.Replace(" ", "");

            if (Rut.Equals("") || Nombre.Equals("") || Apellido.Equals("") || Email.Equals("") || Contrasenia.Equals("") || RepiteContrasenia.Equals(""))
            {
                MessageBox.Show("Complete todos los campos antes de registrar.");
            }
            else
            {
                if (IsValidEmailAddress(txtEmail.Text))
                {
                    if (Contrasenia == RepiteContrasenia)
                    {
                        Negocio.Clases.Usuario u = new Negocio.Clases.Usuario();

                        u.Rut = Rut;
                        u.Nombre = Nombre;
                        u.Apellido = Apellido;
                        u.Email = Email;
                        u.Contrasenia = Contrasenia;
                        u.rol = cbRol.SelectedValue.ToString();

                        Negocio.Clases.RegistroMensaje RM = u.RegistrarUsuario();



                        MessageBox.Show(RM.Mensaje);
                        if (RM.Estado)
                        {
                            txtRut.Text = "";
                            txtNombre.Text = "";
                            txtApellido.Text = "";
                            txtEmail.Text = "";
                            txtContrasenia.Password = "";
                            txtRepiteContrasenia.Password = "";
                            cbRol.SelectedIndex = 0;
                        }

                    }
                    else
                    {
                        MessageBox.Show("Las contraseñas no coinciden.");
                    }
                }
                else
                {
                    MessageBox.Show("Ingrese un email valido");
                }
            }
        }

        private void txtRut_KeyUp(object sender, KeyEventArgs e)
        {
            txtRut.Text = FormatearRut(txtRut.Text);

            txtRut.SelectionStart = txtRut.Text.Length;
            txtRut.SelectionLength = 0;
        }

        public static string FormatearRut(string rut)
        {
            string rutFormateado = string.Empty;

            if (rut.Length == 0)
            {
                rutFormateado = "";
            }
            else
            {
                string rutTemporal;
                string dv;
                Int64 rutNumerico;

                rut = rut.Replace("-", "").Replace(".", "");

                if (rut.Length == 1)
                {
                    rutFormateado = rut;
                }
                else
                {
                    rutTemporal = rut.Substring(0, rut.Length - 1);
                    dv = rut.Substring(rut.Length - 1, 1);

                    //aqui convierto a un numero el RUT si ocurre un error lo deja en CERO
                    if (!Int64.TryParse(rutTemporal, out rutNumerico))
                    {
                        rutNumerico = 0;
                    }

                    //este comando es el que formatea con los separadores de miles
                    rutFormateado = rutNumerico.ToString("N0");

                    if (rutFormateado.Equals("0"))
                    {
                        rutFormateado = string.Empty;
                    }
                    else
                    {
                        //si no hubo problemas con el formateo agrego el DV a la salida
                        rutFormateado += "-" + dv;

                        //y hago este replace por si el servidor tuviese configuracion anglosajona y reemplazo las comas por puntos
                        rutFormateado = rutFormateado.Replace(",", ".");
                    }
                }
            }

            return rutFormateado;
        }

        public bool IsValidEmailAddress(string s)
        {
            Regex regex = new Regex(@"^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$");
            return regex.IsMatch(s);
        }

        private void btnUsuarios_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.Usuarios usuarios = new Ventanas.Usuarios();
            usuarios.ShowDialog();
        }

        private void btnMesas_Click(object sender, RoutedEventArgs e)
        {
            Ventanas.Mesas mesas = new Ventanas.Mesas();
            mesas.ShowDialog();
        }
    }
}
