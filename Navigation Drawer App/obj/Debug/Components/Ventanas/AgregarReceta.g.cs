﻿#pragma checksum "..\..\..\..\Components\Ventanas\AgregarReceta.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "0D79FA9BB4A34C17BA90B50DFD0B897406EC033CD45206A00CB0F58AD7F66659"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Navigation_Drawer_App.Components.Ventanas;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Navigation_Drawer_App.Components.Ventanas {
    
    
    /// <summary>
    /// AgregarReceta
    /// </summary>
    public partial class AgregarReceta : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 61 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image imgSelectedImage;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSeleccionarImagen;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombrePlato;
        
        #line default
        #line hidden
        
        
        #line 95 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtDescripcion;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtPrecio;
        
        #line default
        #line hidden
        
        
        #line 112 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnCancelar;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnAceptar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Navigation Drawer App;component/components/ventanas/agregarreceta.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 34 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
            ((System.Windows.Controls.Grid)(target)).MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.Grid_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.imgSelectedImage = ((System.Windows.Controls.Image)(target));
            return;
            case 3:
            this.btnSeleccionarImagen = ((System.Windows.Controls.Button)(target));
            
            #line 76 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
            this.btnSeleccionarImagen.Click += new System.Windows.RoutedEventHandler(this.btnSeleccionarImagen_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.txtNombrePlato = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.txtDescripcion = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtPrecio = ((System.Windows.Controls.TextBox)(target));
            
            #line 109 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
            this.txtPrecio.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(this.txtPrecio_PreviewTextInput);
            
            #line default
            #line hidden
            return;
            case 7:
            this.btnCancelar = ((System.Windows.Controls.Button)(target));
            
            #line 117 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
            this.btnCancelar.Click += new System.Windows.RoutedEventHandler(this.btnCancelar_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.btnAceptar = ((System.Windows.Controls.Button)(target));
            
            #line 127 "..\..\..\..\Components\Ventanas\AgregarReceta.xaml"
            this.btnAceptar.Click += new System.Windows.RoutedEventHandler(this.btnAceptar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

