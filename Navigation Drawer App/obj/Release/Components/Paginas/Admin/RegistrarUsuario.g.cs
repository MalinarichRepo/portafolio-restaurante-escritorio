﻿#pragma checksum "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "FFDCECF3A1C21A49BF5215233F4F9EEC668FE9508A9A0CCF74596C66CCAEC53E"
//------------------------------------------------------------------------------
// <auto-generated>
//     Este código fue generado por una herramienta.
//     Versión de runtime:4.0.30319.42000
//
//     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
//     se vuelve a generar el código.
// </auto-generated>
//------------------------------------------------------------------------------

using Navigation_Drawer_App.Components.Paginas.Admin;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Navigation_Drawer_App.Components.Paginas.Admin {
    
    
    /// <summary>
    /// RegistrarUsuario
    /// </summary>
    public partial class RegistrarUsuario : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnUsuarios;
        
        #line default
        #line hidden
        
        
        #line 44 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMesas;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border borde;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtRut;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtNombre;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtApellido;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox txtEmail;
        
        #line default
        #line hidden
        
        
        #line 123 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtContrasenia;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.PasswordBox txtRepiteContrasenia;
        
        #line default
        #line hidden
        
        
        #line 133 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbRol;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnRegistrar;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Navigation Drawer App;component/components/paginas/admin/registrarusuario.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.btnUsuarios = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
            this.btnUsuarios.Click += new System.Windows.RoutedEventHandler(this.btnUsuarios_Click);
            
            #line default
            #line hidden
            return;
            case 2:
            this.btnMesas = ((System.Windows.Controls.Button)(target));
            
            #line 50 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
            this.btnMesas.Click += new System.Windows.RoutedEventHandler(this.btnMesas_Click);
            
            #line default
            #line hidden
            return;
            case 3:
            this.borde = ((System.Windows.Controls.Border)(target));
            return;
            case 4:
            this.txtRut = ((System.Windows.Controls.TextBox)(target));
            
            #line 103 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
            this.txtRut.KeyUp += new System.Windows.Input.KeyEventHandler(this.txtRut_KeyUp);
            
            #line default
            #line hidden
            return;
            case 5:
            this.txtNombre = ((System.Windows.Controls.TextBox)(target));
            return;
            case 6:
            this.txtApellido = ((System.Windows.Controls.TextBox)(target));
            return;
            case 7:
            this.txtEmail = ((System.Windows.Controls.TextBox)(target));
            return;
            case 8:
            this.txtContrasenia = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 9:
            this.txtRepiteContrasenia = ((System.Windows.Controls.PasswordBox)(target));
            return;
            case 10:
            this.cbRol = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 11:
            this.btnRegistrar = ((System.Windows.Controls.Button)(target));
            
            #line 144 "..\..\..\..\..\Components\Paginas\Admin\RegistrarUsuario.xaml"
            this.btnRegistrar.Click += new System.Windows.RoutedEventHandler(this.btnRegistrar_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

