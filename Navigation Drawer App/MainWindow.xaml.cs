﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Navigation_Drawer_App
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public int rol;
        public string userid;
        private int op;

        public MainWindow()
        {
            InitializeComponent();
            /*Barra.Content = new Components.Barra_lateral.BarraCocina();*/
        }

        public MainWindow(int op,string userid)
        {
            this.rol = op;
            this.userid = userid;
            InitializeComponent();

            switch (rol)
            {
                case 0:
                    Components.Barra_lateral.BarraAdmin BarraAdmin = new Components.Barra_lateral.BarraAdmin();

                    BarraAdmin.menu = this;

                    Barra.Content = BarraAdmin;
                    break;
                case 1:
                    Components.Barra_lateral.BarraCocina BarraCocina = new Components.Barra_lateral.BarraCocina();

                    BarraCocina.menu = this;

                    Barra.Content = BarraCocina;
                    break;
                case 2:
                    Components.Barra_lateral.BarraBodega BarraBodega = new Components.Barra_lateral.BarraBodega();

                    BarraBodega.menu = this;

                    Barra.Content = BarraBodega;
                    break;
                case 3:
                    Components.Barra_lateral.BarraFinanzas BarraFinanzas = new Components.Barra_lateral.BarraFinanzas();

                    BarraFinanzas.menu = this;

                    Barra.Content = BarraFinanzas;
                    break;
                default:
                    ;
                    break;
            }
        }        


        private void CloseBtn_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnMaxMin_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Maximized)
            {
                this.WindowState = WindowState.Normal;
            }
            else if(this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btnPerfil1_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Contenido.Content = new Components.Paginas.Recetas();
        }

        private void BG_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
    }
}
